use super::intersection::{Intersection, Intersections};
use super::light::Light;
use super::material::Material;
use super::ray;
use super::transformations;
use super::tuple;

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Sphere {
    transform: transformations::Transformation,
    pub material: Material,
}

impl Sphere {
    pub fn new() -> Sphere {
        return Sphere {
            transform: transformations::Transformation::identity(),
            material: Material::new(),
        };
    }

    pub fn intersects(&self, r: ray::Ray) -> Intersections {
        let ray2 = r.transform(self.transform().pseudo_inverse());
        let sphere_to_ray = ray2.origin() - tuple::new_point(0.0, 0.0, 0.0);
        let a = ray2.direction().dot(ray2.direction());
        let b = 2.0 * ray2.direction().dot(sphere_to_ray);
        let c = sphere_to_ray.dot(sphere_to_ray) - 1.0;

        let discriminant = b.powi(2) - 4.0 * a * c;

        if discriminant < 0.0 {
            return Intersections::new(Vec::new());
        } else {
            return Intersections::new(vec![
                Intersection::new((-b - discriminant.sqrt()) / (2.0 * a), *self),
                Intersection::new((-b + discriminant.sqrt()) / (2.0 * a), *self),
            ]);
        }
    }

    pub fn material(&self) -> Material {
        return self.material;
    }

    pub fn set_material(&mut self, material: Material) {
        self.material = material;
    }

    pub fn transform(&self) -> transformations::Transformation {
        return self.transform;
    }

    pub fn set_transform(&mut self, transform: transformations::Transformation) {
        self.transform = transform;
    }

    pub fn normal_at(&self, point: tuple::Tuple) -> tuple::Tuple {
        let object_point = self.transform().pseudo_inverse() * point;
        let object_normal = object_point - tuple::new_point(0.0, 0.0, 0.0);
        let mut world_normal = self.transform().pseudo_inverse().transpose() * object_normal;
        world_normal.set_w(0.0);
        return world_normal.normalize();
    }

    pub fn lighting(
        &self,
        point: tuple::Tuple,
        light: Light,
        eye_vector: tuple::Tuple,
        normal_vector: tuple::Tuple,
    ) {
    }
}

#[cfg(test)]
mod tests {
    use super::ray::Ray;
    use super::transformations;
    use super::tuple::{new_point, new_vector, Tuple};
    use super::Material;
    use super::Sphere;
    use crate::utils::utils;
    use assert_approx_eq::assert_approx_eq;
    use std::f32::consts::PI;

    #[test]
    fn a_sphere_should_have_two_intersections() {
        let r = Ray::new(new_point(0.0, 0.0, -5.0), new_vector(0.0, 0.0, 1.0));
        let sphere = Sphere::new();
        let intersections = sphere.intersects(r);
        assert_approx_eq!(intersections.get(0).t(), 4.0);
        assert_approx_eq!(intersections.get(1).t(), 6.0);
    }
    #[test]
    fn a_sphere_should_have_two_intersections_at_tangent() {
        let r = Ray::new(new_point(0.0, 1.0, -5.0), new_vector(0.0, 0.0, 1.0));
        let sphere = Sphere::new();
        let intersections = sphere.intersects(r);
        assert_approx_eq!(intersections.get(0).t(), 5.0);
        assert_approx_eq!(intersections.get(1).t(), 5.0);
    }
    #[test]
    fn a_sphere_miss_a_sphere() {
        let r = Ray::new(new_point(0.0, 2.0, -5.0), new_vector(0.0, 0.0, 1.0));
        let sphere = Sphere::new();
        let intersections = sphere.intersects(r);
        assert_eq!(intersections.len(), 0);
    }
    #[test]
    fn point_inside_sphere() {
        let r = Ray::new(new_point(0.0, 0.0, 0.0), new_vector(0.0, 0.0, 1.0));
        let sphere = Sphere::new();
        let intersections = sphere.intersects(r);
        assert_approx_eq!(intersections.get(0).t(), -1.0);
        assert_approx_eq!(intersections.get(1).t(), 1.0);
    }
    #[test]
    fn sphere_inside_ray() {
        let r = Ray::new(new_point(0.0, 0.0, 5.0), new_vector(0.0, 0.0, 1.0));
        let sphere = Sphere::new();
        let intersections = sphere.intersects(r);
        assert_approx_eq!(intersections.get(0).t(), -6.0);
        assert_approx_eq!(intersections.get(1).t(), -4.0);
    }

    #[test]
    fn transforming_a_sphere() {
        let mut sphere = Sphere::new();
        let transform = transformations::translate(2.0, 3.0, 4.0);
        sphere.set_transform(transform);
        assert_eq!(sphere.transform(), transform);
    }

    #[test]
    fn intersecting_scaled_ray_with_sphere() {
        let origin = new_point(0.0, 0.0, -5.0);
        let direction = new_vector(0.0, 0.0, 1.0);
        let ray = Ray::new(origin, direction);

        let mut sphere = Sphere::new();
        let transform = transformations::scale(2.0, 2.0, 2.0);
        sphere.set_transform(transform);
        let intersections = sphere.intersects(ray);
        assert_eq!(intersections.len(), 2);
        assert_eq!(intersections.get(0).t(), 3.0);
        assert_eq!(intersections.get(1).t(), 7.0);
    }

    #[test]
    fn intersecting_translated_ray_with_sphere() {
        let origin = new_point(0.0, 0.0, -5.0);
        let direction = new_vector(0.0, 0.0, 1.0);
        let ray = Ray::new(origin, direction);

        let mut sphere = Sphere::new();
        let transform = transformations::translate(5.0, 0.0, 0.0);
        sphere.set_transform(transform);
        let intersections = sphere.intersects(ray);
        assert_eq!(intersections.len(), 0);
    }

    #[test]
    fn the_normal_at_a_point_on_the_x_axis() {
        let s = Sphere::new();
        let n = s.normal_at(new_point(1.0, 0.0, 0.0));
        assert_eq!(n, new_vector(1.0, 0.0, 0.0));
    }
    #[test]
    fn the_nolibrmal_at_a_point_on_the_y_axis() {
        let s = Sphere::new();
        let n = s.normal_at(new_point(0.0, 1.0, 0.0));
        assert_eq!(n, new_vector(0.0, 1.0, 0.0));
    }

    #[test]
    fn the_normal_at_a_point_on_the_z_axis() {
        let s = Sphere::new();
        let n = s.normal_at(new_point(0.0, 0.0, 1.0));
        utils::assert_tuple_equal(n, new_vector(0.0, 0.0, 1.0));
    }

    #[test]
    fn the_normal_at_a_non_axial_point() {
        let s = Sphere::new();
        let n = s.normal_at(new_point(
            3_f32.sqrt() / 3.0,
            3_f32.sqrt() / 3.0,
            3_f32.sqrt() / 3.0,
        ));
        utils::assert_tuple_equal(
            n,
            new_vector(3_f32.sqrt() / 3.0, 3_f32.sqrt() / 3.0, 3_f32.sqrt() / 3.0),
        );
    }

    #[test]
    fn the_normal_at_should_be_a_normal() {
        let s = Sphere::new();
        let n = s.normal_at(new_point(
            3_f32.sqrt() / 3.0,
            3_f32.sqrt() / 3.0,
            3_f32.sqrt() / 3.0,
        ));
        utils::assert_tuple_equal(n, n.normalize());
    }

    #[test]
    fn normal_for_transformed_sphere() {
        let mut s = Sphere::new();
        s.set_transform(transformations::translate(0.0, 1.0, 0.0));
        let n = s.normal_at(new_point(0.0, 1.70711, -0.70711));
        utils::assert_tuple_equal(n, new_vector(0.0, 0.70710695, -0.70710665));
    }

    #[test]
    fn normal_for_transformed_sphere_2() {
        let mut s = Sphere::new();
        let m = transformations::scale(1.0, 0.5, 1.0) * transformations::rotate_z(PI / 5.0);
        s.set_transform(m);
        let n = s.normal_at(new_point(0.0, 2_f32.sqrt() / 2.0, -2_f32.sqrt() / 2.0));
        utils::assert_tuple_equal(n, new_vector(-0.00000018399798, 0.97014254, -0.24253558));
    }

    #[test]
    fn sphere_has_a_material() {
        let s = Sphere::new();
        assert_eq!(s.material(), Material::new());
    }

    #[test]
    fn sphere_can_have_a_material_assigned() {
        let mut s = Sphere::new();
        let mut m = Material::new();
        m.set_diffuse(0.5);
        s.set_material(m);
        assert_eq!(s.material(), m)
    }
}

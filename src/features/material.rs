use super::color::Color;
use super::light::Light;
use super::material;
use super::sphere;
use super::tuple;
use super::tuple::Tuple;

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Material {
    pub color: Color,
    ambient: f32,
    diffuse: f32,
    specular: f32,
    shininess: f32,
}

impl Material {
    pub fn new() -> Material {
        Material {
            color: Color::new(1.0, 1.0, 1.0),
            ambient: 0.1,
            diffuse: 0.9,
            specular: 0.9,
            shininess: 200.0,
        }
    }

    pub fn set_ambient(&mut self, ambient: f32) {
        self.ambient = ambient;
    }
    pub fn set_diffuse(&mut self, difuse: f32) {
        self.diffuse = difuse;
    }
    pub fn set_specular(&mut self, specular: f32) {
        self.specular = specular;
    }
    pub fn set_shininess(&mut self, shininess: f32) {
        self.shininess = shininess;
    }
    pub fn set_color(&mut self, color: Color) {
        self.color = color;
    }

    pub fn lighting(&self, point: Tuple, light: &Light, eyev: Tuple, normal_v: Tuple) -> Color {
        let effective_color = self.color * light.intensity();
        let lightv = (light.position() - point).normalize();
        let ambient = effective_color * self.ambient;
        let mut diffuse;
        let mut specular;

        let light_dot_normal = lightv.dot(normal_v);
        if light_dot_normal < 0.0 {
            diffuse = Color::black();
            specular = Color::black();
        } else {
            diffuse = (effective_color * self.diffuse) * light_dot_normal;
            let reflectv = (-lightv).reflect(normal_v);
            let reflect_dot_eye = reflectv.dot(eyev);
            if reflect_dot_eye <= 0.0 {
                specular = Color::black();
            } else {
                let factor = reflect_dot_eye.powf(self.shininess);
                specular = light.intensity() * self.specular * factor;
            }
        }
        return ambient + diffuse + specular;
    }
}

#[cfg(test)]
mod tests {
    use super::sphere::Sphere;
    use super::tuple::{new_point, new_vector};
    use super::Color;
    use super::Light;
    use super::Material;
    use crate::utils::utils;

    #[test]
    fn the_default_material() {
        let m = Material::new();
        assert_eq!(m.color, Color::new(1.0, 1.0, 1.0));
        assert_eq!(m.ambient, 0.1);
        assert_eq!(m.diffuse, 0.9);
        assert_eq!(m.specular, 0.9);
        assert_eq!(m.shininess, 200.0);
    }

    #[test]
    fn test_lighting() {
        let p = new_point(0.0, 0.0, 0.0);
        let material = Material::new();
        let eyev = new_vector(0.0, 0.0, -1.0);
        let normalv = new_vector(0.0, 0.0, -1.0);
        let light = Light::new(new_point(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let result = material.lighting(p, &light, eyev, normalv);
        assert_eq!(result, Color::new(1.9, 1.9, 1.9));
    }

    #[test]
    fn lighting_between_eye_and_surfance_at_45() {
        let p = new_point(0.0, 0.0, 0.0);
        let material = Material::new();
        let eyev = new_vector(0.0, 2_f32.sqrt() / 2.0, -2_f32.sqrt() / 2.0);
        let normalv = new_vector(0.0, 0.0, -1.0);
        let light = Light::new(new_point(0.0, 0.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let result = material.lighting(p, &light, eyev, normalv);
        utils::assert_tuple_equal(result.tuple(), Color::new(1.0, 1.0, 1.0).tuple());
    }

    #[test]
    fn lighting_between_eye_and_light_at_45() {
        let p = new_point(0.0, 0.0, 0.0);
        let material = Material::new();
        let eyev = new_vector(0.0, 0.0, -1.0);
        let normalv = new_vector(0.0, 0.0, -1.0);
        let light = Light::new(new_point(0.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let result = material.lighting(p, &light, eyev, normalv);
        utils::assert_tuple_equal(result.tuple(), Color::new(0.7364, 0.7364, 0.7364).tuple());
    }

    #[test]
    fn lighting_between_eye_in_reflection_vector_path() {
        let p = new_point(0.0, 0.0, 0.0);
        let material = Material::new();
        let eyev = new_vector(0.0, -2_f32.sqrt() / 2.0, -2_f32.sqrt() / 2.0);
        let normalv = new_vector(0.0, 0.0, -1.0);
        let light = Light::new(new_point(0.0, 10.0, -10.0), Color::new(1.0, 1.0, 1.0));
        let result = material.lighting(p, &light, eyev, normalv);
        utils::assert_tuple_equal(result.tuple(), Color::new(1.6364, 1.6364, 1.6364).tuple());
    }

    #[test]
    fn lighting_behind_surfance() {
        let p = new_point(0.0, 0.0, 0.0);
        let material = Material::new();
        let eyev = new_vector(0.0, 0.0, -1.0);
        let normalv = new_vector(0.0, 0.0, -1.0);
        let light = Light::new(new_point(0.0, 0.0, 10.0), Color::new(1.0, 1.0, 1.0));
        let result = material.lighting(p, &light, eyev, normalv);
        assert_eq!(result, Color::new(0.1, 0.1, 0.1));
    }
}

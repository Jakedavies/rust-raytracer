use nalgebra::Vector4;
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Copy, Clone, Debug)]
pub struct Tuple(pub Vector4<f32>);

impl Tuple {
    pub fn from_vec4(vec4: Vector4<f32>) -> Tuple {
        return Tuple(vec4);
    }
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Tuple {
        return Tuple(Vector4::<f32>::new(x, y, z, w));
    }

    pub fn x(&self) -> f32 {
        return self.0.as_slice()[0];
    }

    pub fn y(&self) -> f32 {
        return self.0.as_slice()[1];
    }

    pub fn z(&self) -> f32 {
        return self.0.as_slice()[2];
    }

    pub fn w(&self) -> f32 {
        return self.0.as_slice()[3];
    }

    pub fn set_w(&mut self, w: f32) {
        self.0[3] = w;
    }

    pub fn magnitude(&self) -> f32 {
        return (self.x().powi(2) + self.y().powi(2) + self.z().powi(2)).sqrt();
    }

    pub fn normalize(&self) -> Tuple {
        let magnitude = self.magnitude();
        return Tuple::new(
            self.x() / magnitude,
            self.y() / magnitude,
            self.z() / magnitude,
            self.w(),
        );
    }

    pub fn dot(&self, b: Tuple) -> f32 {
        return self.x() * b.x() + self.y() * b.y() + self.z() * b.z() + self.w() * b.w();
    }

    pub fn cross(&self, b: Tuple) -> Tuple {
        return new_vector(
            self.y() * b.z() - self.z() * b.y(),
            self.z() * b.x() - self.x() * b.z(),
            self.x() * b.y() - self.y() * b.x(),
        );
    }

    pub fn reflect(&self, normal: Tuple) -> Tuple {
        return *self - normal * 2.0 * self.dot(normal);
    }
}

impl PartialEq for Tuple {
    fn eq(&self, other: &Tuple) -> bool {
        self.0 == other.0
    }
}

impl std::ops::Index<(usize, usize)> for Tuple {
    type Output = f32;

    fn index(&self, idx: (usize, usize)) -> &f32 {
        // or as appropriate for row- or column-major data
        &self.0[(idx.0, idx.1)]
    }
}

impl Neg for Tuple {
    type Output = Tuple;

    fn neg(self) -> Tuple {
        return Tuple::new(-self.x(), -self.y(), -self.z(), -self.w());
    }
}

impl Add<Tuple> for Tuple {
    type Output = Tuple;
    fn add(self, rhs: Tuple) -> Tuple {
        return Tuple::new(
            self.x() + rhs.x(),
            self.y() + rhs.y(),
            self.z() + rhs.z(),
            self.w() + rhs.w(),
        );
    }
}

impl Sub<Tuple> for Tuple {
    type Output = Tuple;
    fn sub(self, rhs: Tuple) -> Tuple {
        return Tuple::new(
            self.x() - rhs.x(),
            self.y() - rhs.y(),
            self.z() - rhs.z(),
            self.w() - rhs.w(),
        );
    }
}

impl Mul<f32> for Tuple {
    type Output = Tuple;
    fn mul(self, rhs: f32) -> Tuple {
        return Tuple::new(
            self.x() * rhs,
            self.y() * rhs,
            self.z() * rhs,
            self.w() * rhs,
        );
    }
}

impl Mul<Tuple> for Tuple {
    type Output = Tuple;
    fn mul(self, rhs: Tuple) -> Tuple {
        return Tuple::new(
            self.x() * rhs.x(),
            self.y() * rhs.y(),
            self.z() * rhs.z(),
            self.w() * rhs.w(),
        );
    }
}

impl Div<f32> for Tuple {
    type Output = Tuple;
    fn div(self, rhs: f32) -> Tuple {
        return Tuple::new(
            self.x() / rhs,
            self.y() / rhs,
            self.z() / rhs,
            self.w() / rhs,
        );
    }
}

pub fn new_point(x: f32, y: f32, z: f32) -> Tuple {
    return Tuple::new(x, y, z, 1.0);
}

pub fn new_vector(x: f32, y: f32, z: f32) -> Tuple {
    return Tuple::new(x, y, z, 0.0);
}

#[cfg(test)]
mod tests {
    use super::{new_point, new_vector, Tuple};
    use crate::utils;

    #[test]
    fn new_tuple_wtrue() {
        let tuple = Tuple::new(4.3, -4.1, 3.1, 1.0);
        assert_eq!(tuple.x(), 4.3);
        assert_eq!(tuple.y(), -4.1);
        assert_eq!(tuple.z(), 3.1);
        assert_eq!(tuple.w(), 1.0);
    }
    #[test]
    fn new_tuple_wfalse() {
        let tuple = Tuple::new(4.3, -4.2, 3.1, 0.0);
        assert_eq!(tuple.x(), 4.3);
        assert_eq!(tuple.y(), -4.2);
        assert_eq!(tuple.z(), 3.1);
        assert_eq!(tuple.w(), 0.0);
    }

    #[test]
    fn new_point_helper() {
        let point = new_point(4.3, -4.1, 3.1);
        assert_eq!(point.x(), 4.3);
        assert_eq!(point.y(), -4.1);
        assert_eq!(point.z(), 3.1);
        assert_eq!(point.w(), 1.0);
    }

    #[test]
    fn new_vector_helper() {
        let vector = new_vector(4.3, -4.1, 3.1);
        assert_eq!(vector.x(), 4.3);
        assert_eq!(vector.y(), -4.1);
        assert_eq!(vector.z(), 3.1);
        assert_eq!(vector.w(), 0.0);
    }

    #[test]
    fn adding_two_tuples() {
        let v1 = new_vector(1.0, 2.0, 3.0);
        let v2 = new_point(6.0, 4.0, 2.0);
        let result = v1 + v2;
        assert_eq!(result.x(), 7.0);
        assert_eq!(result.y(), 6.0);
        assert_eq!(result.z(), 5.0);
        assert_eq!(result.w(), 1.0);
    }

    #[test]
    fn subtracting_two_points() {
        let v1 = new_point(1.0, 2.0, 3.0);
        let v2 = new_point(6.0, 4.0, 2.0);

        let result = v1 - v2;

        assert_eq!(result.x(), -5.0);
        assert_eq!(result.y(), -2.0);
        assert_eq!(result.z(), 1.0);
        assert_eq!(result.w(), 0.0);
    }

    #[test]
    fn subtracting_vector_from_point() {
        let v1 = new_point(1.0, 2.0, 3.0);
        let v2 = new_vector(6.0, 4.0, 2.0);
        let result = v1 - v2;

        assert_eq!(result.x(), -5.0);
        assert_eq!(result.y(), -2.0);
        assert_eq!(result.z(), 1.0);
        assert_eq!(result.w(), 1.0);
    }

    #[test]
    fn subtracting_two_vectors() {
        let v1 = new_vector(1.0, 2.0, 3.0);
        let v2 = new_vector(6.0, 4.0, 2.0);
        let result = v1 - v2;
        assert_eq!(result.x(), -5.0);
        assert_eq!(result.y(), -2.0);
        assert_eq!(result.z(), 1.0);
        assert_eq!(result.w(), 0.0);
    }

    #[test]
    fn negating_a_tuple() {
        let neg = -new_point(1.0, 2.0, 3.0);
        assert_eq!(neg.x(), -1.0);
        assert_eq!(neg.y(), -2.0);
        assert_eq!(neg.z(), -3.0);
        assert_eq!(neg.w(), -1.0);
    }

    #[test]
    fn vector_times_a_scaler() {
        let v1 = new_vector(1.0, 2.0, 3.0);
        let result = v1 * 5.0;
        assert_eq!(result.x(), 5.0);
        assert_eq!(result.y(), 10.0);
        assert_eq!(result.z(), 15.0);
        assert_eq!(result.w(), 0.0);
    }

    #[test]
    fn vector_divided_by_scaler() {
        let v1 = new_vector(6.0, 4.0, 2.0);
        let result = v1 / 2.0;
        assert_eq!(result.x(), 3.0);
        assert_eq!(result.y(), 2.0);
        assert_eq!(result.z(), 1.0);
        assert_eq!(result.w(), 0.0);
    }

    #[test]
    fn magnitude_1() {
        assert_eq!(new_vector(1.0, 0.0, 0.0).magnitude(), 1.0);
    }

    #[test]
    fn magnitude_2() {
        assert_eq!(new_vector(0.0, 1.0, 0.0).magnitude(), 1.0);
    }

    #[test]
    fn magnitude_3() {
        assert_eq!(new_vector(0.0, 0.0, 1.0).magnitude(), 1.0);
    }

    #[test]
    fn magnitude_4() {
        assert_eq!(new_vector(1.0, 2.0, 3.0).magnitude(), (14.0 as f32).sqrt());
    }

    #[test]
    fn magnitude_5() {
        assert_eq!(
            new_vector(-1.0, -2.0, -3.0).magnitude(),
            (14.0 as f32).sqrt()
        );
    }

    #[test]
    fn normalize_vector() {
        let result = new_vector(4.0, 0.0, 0.0).normalize();
        assert_eq!(result.x(), 1.0);
        assert_eq!(result.y(), 0.0);
        assert_eq!(result.z(), 0.0);
    }

    #[test]
    fn normalize_vector_2() {
        let result = new_vector(1.0, 2.0, 3.0).normalize();
        assert_eq!(result.x(), 1.0 / (14 as f32).sqrt());
        assert_eq!(result.y(), 2.0 / (14 as f32).sqrt());
        assert_eq!(result.z(), 3.0 / (14 as f32).sqrt());
    }

    #[test]
    fn normalized_vector_magnitude() {
        let result = new_vector(1.0, 2.0, 3.0).normalize();
        assert_eq!(result.magnitude().round(), 1.0);
    }

    #[test]
    fn dot_product() {
        let v1 = new_vector(1.0, 2.0, 3.0);
        let v2 = new_vector(2.0, 3.0, 4.0);
        assert_eq!(v1.dot(v2), 20.0);
    }

    #[test]
    fn cross_product() {
        let v1 = new_vector(1.0, 2.0, 3.0);
        let v2 = new_vector(2.0, 3.0, 4.0);
        let cross = v1.cross(v2);
        let other_cross = v2.cross(v1);

        assert_eq!(cross.x(), -1.0);
        assert_eq!(cross.y(), 2.0);
        assert_eq!(cross.z(), -1.0);

        assert_eq!(other_cross.x(), 1.0);
        assert_eq!(other_cross.y(), -2.0);
        assert_eq!(other_cross.z(), 1.0);
    }

    #[test]
    fn reflecting_vector_at_45() {
        let v = new_vector(1.0, -1.0, 0.0);
        let n = new_vector(0.0, 1.0, 0.0);
        let r = v.reflect(n);
        assert_eq!(r, new_vector(1.0, 1.0, 0.0));
    }

    #[test]
    fn reflecting_vector_off_slanted_surface() {
        let v = new_vector(0.0, -1.0, 0.0);
        let n = new_vector(2_f32.sqrt() / 2.0, 2_f32.sqrt() / 2.0, 0.0);
        let r = v.reflect(n);
        utils::utils::assert_tuple_equal(r, new_vector(1.0, 0.0, 0.0));
    }
}

use super::sphere::Sphere;
use std::f32::MAX;

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Intersection {
    t: f32,
    object: Sphere,
}

impl Intersection {
    pub fn new(t: f32, object: Sphere) -> Intersection {
        return Intersection {
            t: t,
            object: object,
        };
    }

    pub fn t(&self) -> f32 {
        self.t
    }

    pub fn object(&self) -> Sphere {
        self.object
    }
}

pub struct Intersections(pub Vec<Intersection>);

impl Intersections {
    pub fn new(intersections: Vec<Intersection>) -> Intersections {
        return Intersections(intersections);
    }
    pub fn get(&self, index: usize) -> Intersection {
        self.0[index]
    }

    pub fn len(&self) -> usize {
        return self.0.len();
    }

    pub fn hit(&self) -> Option<Intersection> {
        let mut min = MAX;
        let mut current = None;
        for i in self.0.iter() {
            if (i.t() < min && i.t() >= 0.0) {
                current = Some(*i);
                min = i.t();
            }
        }
        return current;
    }
}

#[cfg(test)]
mod tests {
    use super::{Intersection, Intersections, Sphere};

    #[test]
    fn encapsulates_t_and_object() {
        let s = Sphere::new();
        let i = Intersection::new(3.5, s);

        assert_eq!(i.object(), s);
        assert_eq!(i.t(), 3.5);
    }

    #[test]
    fn aggregate_intersections() {
        let s = Sphere::new();
        let i1 = Intersection::new(1.0, s);
        let i2 = Intersection::new(2.0, s);
        let is = Intersections::new(vec![i1, i2]);
        assert_eq!(is.len(), 2);
        assert_eq!(is.get(0).t(), 1.0);
        assert_eq!(is.get(1).t(), 2.0);
    }

    #[test]
    fn the_hit_for_positive_t_intersections() {
        let s = Sphere::new();
        let i1 = Intersection::new(1.0, s);
        let i2 = Intersection::new(2.0, s);
        let intersections = Intersections::new(vec![i1, i2]);
        let hit = intersections.hit();
        match hit {
            None => assert!(false),
            Some(hit) => assert_eq!(hit, i1),
        }
    }
    #[test]
    fn the_hit_for_negative_t_intersections() {
        let s = Sphere::new();
        let i1 = Intersection::new(-1.0, s);
        let i2 = Intersection::new(2.0, s);
        let intersections = Intersections::new(vec![i1, i2]);
        let hit = intersections.hit();
        match hit {
            None => assert!(false),
            Some(hit) => assert_eq!(hit, i2),
        }
    }
    #[test]
    fn the_hit_for_all_negative_t() {
        let s = Sphere::new();
        let i1 = Intersection::new(-1.0, s);
        let i2 = Intersection::new(-2.0, s);
        let intersections = Intersections::new(vec![i1, i2]);
        let hit = intersections.hit();
        match hit {
            None => assert!(true),
            Some(hit) => assert!(false),
        }
    }

    #[test]
    fn the_hit_is_the_lowest_non_neg_intersection() {
        let s = Sphere::new();
        let i1 = Intersection::new(5.0, s);
        let i2 = Intersection::new(7.0, s);
        let i3 = Intersection::new(-3.0, s);
        let i4 = Intersection::new(2.0, s);
        let intersections = Intersections::new(vec![i1, i2, i3, i4]);
        let hit = intersections.hit();
        match hit {
            None => assert!(true),
            Some(hit) => assert_eq!(i4, hit),
        }
    }
}

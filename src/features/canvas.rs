use super::color::Color;

fn float_to_color(color: f32) -> i32 {
    let scaled = color * 255.0;
    return scaled.max(0.0).min(255.0).round() as i32;
}

pub struct Canvas {
    width: u32,
    height: u32,
    pixels: Vec<Color>,
}

impl Canvas {
    pub fn new(width: u32, height: u32) -> Canvas {
        Canvas {
            width: width,
            height: height,
            pixels: vec![Color::new(0.0, 0.0, 0.0); (width * height) as usize],
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    fn get_pixel_index(&self, x: u32, y: u32) -> u32 {
        return y * self.width() + x;
    }

    pub fn pixel_at(&self, x: u32, y: u32) -> Color {
        let index = self.get_pixel_index(x, y);
        return self.pixels[index as usize];
    }

    pub fn write_pixel(&mut self, x: u32, y: u32, color: Color) {
        let index = self.get_pixel_index(x, y);
        self.pixels[index as usize] = color;
    }

    pub fn to_ppm(&self) -> String {
        let mut content: String = format!("P3\n{} {}\n255\n", self.width(), self.height());
        let mut pixel: Color;
        for i in 0..self.pixels.len() {
            pixel = self.pixels[i];

            content.push_str(&format!(
                "{} {} {}\n",
                float_to_color(pixel.r()),
                float_to_color(pixel.g()),
                float_to_color(pixel.b()),
            ));
        }

        content.push_str(&format!("\n",));

        return content;
    }
}

#[cfg(test)]
mod tests {
    use super::Canvas;
    use super::Color;

    #[test]
    fn creating_a_canvas() {
        let canvas = Canvas::new(10, 15);
        for x in 0..10 {
            for y in 0..15 {
                let pixel = canvas.pixel_at(x, y);
                assert_eq!(pixel.r(), 0.0);
                assert_eq!(pixel.g(), 0.0);
                assert_eq!(pixel.b(), 0.0);
            }
        }
        assert_eq!(canvas.width(), 10);
        assert_eq!(canvas.height(), 15);
    }

    #[test]
    fn write_a_pixel() {
        let mut canvas = Canvas::new(10, 15);

        let pixel = canvas.pixel_at(2, 3);
        assert_eq!(pixel.r(), 0.0);
        assert_eq!(pixel.g(), 0.0);
        assert_eq!(pixel.b(), 0.0);

        canvas.write_pixel(2, 3, Color::new(0.5, 0.6, 0.3));

        let pixel2 = canvas.pixel_at(2, 3);
        assert_eq!(pixel2.r(), 0.5);
        assert_eq!(pixel2.g(), 0.6);
        assert_eq!(pixel2.b(), 0.3);
    }

    #[test]
    fn to_ppm_header() {
        let canvas = Canvas::new(5, 3);
        let text = canvas.to_ppm();
        let mut lines = text.lines();
        assert_eq!(lines.next(), Some("P3"));
        assert_eq!(lines.next(), Some("5 3"));
        assert_eq!(lines.next(), Some("255"));
    }

    #[test]
    fn constructing_ppm() {
        let mut canvas = Canvas::new(5, 3);
        canvas.write_pixel(0, 0, Color::new(1.5, 0.0, 0.0));
        canvas.write_pixel(2, 1, Color::new(0.0, 0.5, 0.0));
        canvas.write_pixel(4, 2, Color::new(-0.5, 0.0, 1.0));
        let text = canvas.to_ppm();
        let mut lines = text.lines();
        assert_eq!(lines.nth(3), Some("255 0 0"));
        assert_eq!(lines.nth(6), Some("0 128 0"));
        assert_eq!(lines.nth(6), Some("0 0 255"));
        assert_eq!(lines.last(), Some(""));
    }
}

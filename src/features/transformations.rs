use super::tuple;
use super::tuple::Tuple;
use nalgebra::{Matrix4, Vector4};
use std::f64::consts::PI;
use std::ops::{Index, Mul};

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Transformation(pub Matrix4<f32>);

impl Mul<Tuple> for Transformation {
  type Output = Tuple;
  fn mul(self, rhs: Tuple) -> Tuple {
    return Tuple::from_vec4(self.0 * rhs.0);
  }
}

impl Mul<Transformation> for Transformation {
  type Output = Transformation;
  fn mul(self, rhs: Transformation) -> Transformation {
    return Transformation::from_matrix(self.0 * rhs.0);
  }
}

impl Transformation {
  pub fn from_matrix(mat: Matrix4<f32>) -> Transformation {
    return Transformation(mat);
  }

  pub fn pseudo_inverse(&self) -> Transformation {
    return Transformation::from_matrix(self.0.pseudo_inverse(0.001));
  }

  pub fn identity() -> Transformation {
    return Transformation::from_matrix(Matrix4::identity());
  }

  pub fn transpose(&self) -> Transformation {
    return Transformation::from_matrix(self.0.transpose());
  }
}

pub fn translate(x: f32, y: f32, z: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  matrix.set_column(3, &Vector4::new(x, y, z, 1.0));
  return Transformation::from_matrix(matrix);
}

pub fn scale(x: f32, y: f32, z: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  matrix.set_diagonal(&Vector4::new(x, y, z, 1.0));
  return Transformation::from_matrix(matrix);
}

pub fn rotate_x(radians: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  let cos_rads = radians.cos();
  let sin_rads = radians.sin();
  matrix.set_column(1, &Vector4::new(0.0, cos_rads, sin_rads, 0.0));
  matrix.set_column(2, &Vector4::new(0.0, -sin_rads, cos_rads, 0.0));
  return Transformation::from_matrix(matrix);
}

pub fn rotate_y(radians: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  let cos_rads = radians.cos();
  let sin_rads = radians.sin();
  matrix.set_column(0, &Vector4::new(cos_rads, 0.0, -sin_rads, 0.0));
  matrix.set_column(2, &Vector4::new(sin_rads, 0.0, cos_rads, 0.0));
  return Transformation::from_matrix(matrix);
}

pub fn rotate_z(radians: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  let cos_rads = radians.cos();
  let sin_rads = radians.sin();
  matrix.set_column(0, &Vector4::new(cos_rads, sin_rads, 0.0, 0.0));
  matrix.set_column(1, &Vector4::new(-sin_rads, cos_rads, 0.0, 0.0));
  return Transformation::from_matrix(matrix);
}

pub fn shear(xy: f32, xz: f32, yx: f32, yz: f32, zx: f32, zy: f32) -> Transformation {
  let mut matrix = Matrix4::identity();
  matrix.set_column(0, &Vector4::new(1.0, yx, zx, 0.0));
  matrix.set_column(1, &Vector4::new(xy, 1.0, zy, 0.0));
  matrix.set_column(2, &Vector4::new(xz, yz, 1.0, 0.0));
  return Transformation::from_matrix(matrix);
}

#[cfg(test)]
mod tests {
  use super::tuple::{new_point, new_vector};
  use super::PI;
  use super::{rotate_x, rotate_y, rotate_z, scale, shear, translate};
  use assert_approx_eq::assert_approx_eq;

  #[test]
  fn multiplying_by_translation_matrix() {
    let transform = translate(5.0, -3.0, 2.);
    let point = transform * new_point(-3.0, 4.0, 5.0);
    assert_approx_eq!(point.x(), 2.0, 0.001);
    assert_approx_eq!(point.y(), 1.0, 0.001);
    assert_approx_eq!(point.z(), 7.0, 0.001);
  }

  #[test]
  fn multiplying_by_the_inverse_translation_matrix() {
    let transform = translate(5.0, -3.0, 2.);
    let point = new_point(-3.0, 4.0, 5.0);
    let result = transform.pseudo_inverse() * point;
    assert_approx_eq!(result.x(), -8.0, 0.001);
    assert_approx_eq!(result.y(), 7.0, 0.001);
    assert_approx_eq!(result.z(), 3.0, 0.001);
  }

  #[test]
  fn translation_does_not_affect_vectors() {
    let transform = translate(5.0, -3.0, 2.0);
    let vector = new_vector(-3.0, 4.0, 5.0);
    let result = transform * vector;
    assert_approx_eq!(result.x(), -3.0);
    assert_approx_eq!(result.y(), 4.0, 0.001);
    assert_approx_eq!(result.z(), 5.0, 0.001);
  }

  #[test]
  fn scale_a_vector() {
    let scaling = scale(2.0, 3.0, 4.0);
    let point = new_vector(-4.0, 6.0, 8.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), -8.0);
    assert_approx_eq!(result.y(), 18.0);
    assert_approx_eq!(result.z(), 32.0);
  }

  #[test]
  fn scale_a_point() {
    let scaling = scale(2.0, 3.0, 4.0);
    let point = new_point(-4.0, 6.0, 8.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), -8.0);
    assert_approx_eq!(result.y(), 18.0);
    assert_approx_eq!(result.z(), 32.0);
  }

  #[test]
  fn scale_a_point_by_the_invserse_scale() {
    let scaling = scale(2.0, 3.0, 4.0).pseudo_inverse();
    let point = new_point(-4.0, 6.0, 8.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), -2.0);
    assert_approx_eq!(result.y(), 2.0);
    assert_approx_eq!(result.z(), 2.0);
  }

  #[test]
  fn scaling_by_a_negative_for_reflection() {
    let scaling = scale(-1.0, 1.0, 1.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), -2.0);
    assert_approx_eq!(result.y(), 3.0);
    assert_approx_eq!(result.z(), 4.0);
  }

  #[test]
  fn rotating_a_point_around_x_axis() {
    let point = new_point(0.0, 1.0, 0.0);
    let half_quarter_rotate = rotate_x(PI as f32 / 4.0);
    let full_quarter_rotate = rotate_x(PI as f32 / 2.0);
    let half_quarter = half_quarter_rotate * point;
    let full_quarter = full_quarter_rotate * point;
    assert_approx_eq!(half_quarter.x(), 0.0);
    assert_approx_eq!(half_quarter.y(), (2.0_f32).sqrt() / 2.0);
    assert_approx_eq!(half_quarter.z(), (2.0_f32).sqrt() / 2.0);

    assert_approx_eq!(full_quarter.x(), 0.0);
    assert_approx_eq!(full_quarter.y(), 0.0);
    assert_approx_eq!(full_quarter.z(), 1.0);
  }

  #[test]
  fn rotating_a_point_around_x_axis_inverse() {
    let point = new_point(0.0, 1.0, 0.0);
    let half_quarter_rotate = rotate_x(PI as f32 / 4.0).pseudo_inverse();
    let half_quarter = half_quarter_rotate * point;
    assert_approx_eq!(half_quarter.x(), 0.0);
    assert_approx_eq!(half_quarter.y(), (2.0_f32).sqrt() / 2.0);
    assert_approx_eq!(half_quarter.z(), -(2.0_f32).sqrt() / 2.0);
  }

  #[test]
  fn rotating_a_point_around_y_axis() {
    let point = new_point(0.0, 0.0, 1.0);
    let half_quarter_rotate = rotate_y(PI as f32 / 4.0);
    let full_quarter_rotate = rotate_y(PI as f32 / 2.0);
    let half_quarter = half_quarter_rotate * point;
    let full_quarter = full_quarter_rotate * point;
    assert_approx_eq!(half_quarter.x(), (2.0_f32).sqrt() / 2.0);
    assert_approx_eq!(half_quarter.y(), 0.0);
    assert_approx_eq!(half_quarter.z(), (2.0_f32).sqrt() / 2.0);

    assert_approx_eq!(full_quarter.x(), 1.0);
    assert_approx_eq!(full_quarter.y(), 0.0);
    assert_approx_eq!(full_quarter.z(), 0.0);
  }

  #[test]
  fn rotating_a_point_around_z_axis() {
    let point = new_point(0.0, 1.0, 0.0);
    let half_quarter_rotate = rotate_z(PI as f32 / 4.0);
    let full_quarter_rotate = rotate_z(PI as f32 / 2.0);
    let half_quarter = half_quarter_rotate * point;
    let full_quarter = full_quarter_rotate * point;
    assert_approx_eq!(half_quarter.x(), -(2.0_f32).sqrt() / 2.0);
    assert_approx_eq!(half_quarter.y(), (2.0_f32).sqrt() / 2.0);
    assert_approx_eq!(half_quarter.z(), 0.0);

    assert_approx_eq!(full_quarter.x(), -1.0);
    assert_approx_eq!(full_quarter.y(), 0.0);
    assert_approx_eq!(full_quarter.z(), 0.0);
  }

  #[test]
  fn shearing_moves_x_in_propotion_to_y() {
    let scaling = shear(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 5.0);
    assert_approx_eq!(result.y(), 3.0);
    assert_approx_eq!(result.z(), 4.0)
  }

  #[test]
  fn shearing_moves_x_in_propotion_to_z() {
    let scaling = shear(0.0, 1.0, 0.0, 0.0, 0.0, 0.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 6.0);
    assert_approx_eq!(result.y(), 3.0);
    assert_approx_eq!(result.z(), 4.0);
  }
  #[test]
  fn shearing_moves_y_in_propotion_to_x() {
    let scaling = shear(0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 2.0);
    assert_approx_eq!(result.y(), 5.0);
    assert_approx_eq!(result.z(), 4.0);
  }
  #[test]
  fn shearing_moves_y_in_propotion_to_z() {
    let scaling = shear(0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 2.0);
    assert_approx_eq!(result.y(), 7.0);
    assert_approx_eq!(result.z(), 4.0);
  }
  #[test]
  fn shearing_moves_z_in_propotion_to_x() {
    let scaling = shear(0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 2.0);
    assert_approx_eq!(result.y(), 3.0);
    assert_approx_eq!(result.z(), 6.0);
  }
  #[test]
  fn shearing_moves_z_in_propotion_to_y() {
    let scaling = shear(0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    let point = new_point(2.0, 3.0, 4.0);
    let result = scaling * point;
    assert_approx_eq!(result.x(), 2.0);
    assert_approx_eq!(result.y(), 3.0);
    assert_approx_eq!(result.z(), 7.0);
  }

  #[test]
  fn point_transformations_are_applied_in_sequence() {
    let point = new_point(1.0, 0.0, 1.0);
    let rotation = rotate_x(PI as f32 / 2.0);
    let scaling = scale(5.0, 5.0, 5.0);
    let translation = translate(10.0, 5.0, 7.0);
    let final_point = (translation * rotation * scaling) * point;

    assert_approx_eq!(final_point.x(), 15.0);
    assert_approx_eq!(final_point.y(), 0.0);
    assert_approx_eq!(final_point.z(), 7.0);
  }
}

use super::tuple::Tuple;
use std::ops::{Add, Mul, Neg, Sub};

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Color {
    tuple: Tuple,
}

impl Color {
    pub fn new(r: f32, g: f32, b: f32) -> Color {
        Color {
            tuple: Tuple::new(r, g, b, 0.0),
        }
    }
    pub fn black() -> Color {
        Color {
            tuple: Tuple::new(0.0, 0.0, 0.0, 0.0),
        }
    }

    pub fn r(&self) -> f32 {
        return self.tuple.x();
    }
    pub fn g(&self) -> f32 {
        return self.tuple.y();
    }
    pub fn b(&self) -> f32 {
        return self.tuple.z();
    }

    pub fn tuple(&self) -> Tuple {
        return self.tuple;
    }
}
impl Neg for Color {
    type Output = Color;
    fn neg(self) -> Color {
        return Color { tuple: -self.tuple };
    }
}

impl Add<Color> for Color {
    type Output = Color;
    fn add(self, rhs: Color) -> Color {
        return Color {
            tuple: self.tuple + rhs.tuple,
        };
    }
}

impl Sub<Color> for Color {
    type Output = Color;
    fn sub(self, rhs: Color) -> Color {
        return Color {
            tuple: self.tuple - rhs.tuple,
        };
    }
}

impl Mul<Color> for Color {
    type Output = Color;
    fn mul(self, rhs: Color) -> Color {
        return Color {
            tuple: self.tuple * rhs.tuple,
        };
    }
}

impl Mul<f32> for Color {
    type Output = Color;
    fn mul(self, rhs: f32) -> Color {
        return Color {
            tuple: Tuple::new(
                self.tuple.x() * rhs,
                self.tuple.y() * rhs,
                self.tuple.z() * rhs,
                0.0,
            ),
        };
    }
}

#[cfg(test)]
mod tests {
    use super::Color;

    #[test]
    fn new_color() {
        let color = Color::new(0.5, -0.3, 1.0);
        assert_eq!(color.r(), 0.5);
        assert_eq!(color.g(), -0.3);
        assert_eq!(color.b(), 1.0);
    }

    #[test]
    fn add_colors() {
        let color = Color::new(0.5, -0.3, 1.0);
        let color2 = Color::new(0.3, -0.6, -0.3);
        let result = color + color2;
        assert!(result.r() - 0.8 < 0.001);
        assert!(result.g() + 0.9 < 0.001);
        assert!(result.b() - 0.7 < 0.001);
    }

    #[test]
    fn sub_colors() {
        let color = Color::new(0.5, -0.3, 1.0);
        let color2 = Color::new(0.3, -0.6, -0.3);
        let result = color - color2;
        assert!(result.r() - 0.2 < 0.0001);
        assert!(result.g() - 0.3 < 0.0001);
        assert!(result.b() - 1.3 < 0.0001);
    }

    #[test]
    fn multiply_color_by_scaler() {
        let color = Color::new(0.5, -0.3, 1.0);
        let scaled = color * 2.0;
        assert!(scaled.r() - 1.0 < 0.001);
        assert!(scaled.g() + 0.6 < 0.001);
        assert!(scaled.b() - 2.0 < 0.001);
    }

    #[test]
    fn multiply_color_by_color() {
        let color = Color::new(0.5, -0.3, 1.0);
        let color2 = Color::new(0.3, -0.6, -0.3);
        let result = color * color2;
        assert!(result.r() - 1.5 < 0.001);
        assert!(result.g() - 1.8 < 0.001);
        assert!(result.b() + 0.3 < 0.001);
    }
}

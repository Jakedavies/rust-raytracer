use super::transformations;
use super::tuple;
use super::tuple::Tuple;

#[derive(Copy, Clone, Debug)]
pub struct Ray {
    origin: Tuple,
    direction: Tuple,
}

impl Ray {
    pub fn new(origin: Tuple, direction: Tuple) -> Ray {
        return Ray {
            origin: origin,
            direction: direction,
        };
    }

    pub fn origin(&self) -> Tuple {
        return self.origin;
    }

    pub fn direction(&self) -> Tuple {
        return self.direction;
    }

    pub fn position_at(&self, t: f32) -> Tuple {
        return self.origin + self.direction * t;
    }

    pub fn transform(&self, transformation: transformations::Transformation) -> Ray {
        return Ray {
            origin: transformation * self.origin(),
            direction: transformation * self.direction(),
        };
    }
}

#[cfg(test)]
mod tests {
    use super::transformations;
    use super::tuple::{new_point, new_vector};
    use super::Ray;

    #[test]
    fn creating_and_querying_a_ray() {
        let origin = new_point(1.0, 2.0, 3.0);
        let direction = new_vector(4.0, 5.0, 6.0);
        let ray = Ray::new(origin, direction);
        assert_eq!(ray.origin(), origin);
        assert_eq!(ray.direction(), direction);
    }

    #[test]
    fn compute_a_point_from_distance() {
        let origin = new_point(1.0, 2.0, 3.0);
        let direction = new_vector(1.0, 0.0, 0.0);
        let ray = Ray::new(origin, direction);
        let p1 = ray.position_at(1.0);
        let p2 = ray.position_at(2.0);
        let p3 = ray.position_at(-1.0);
        let p4 = ray.position_at(2.5);
        assert_eq!(p1, new_point(2.0, 2.0, 3.0));
        assert_eq!(p2, new_point(3.0, 2.0, 3.0));
        assert_eq!(p3, new_point(0.0, 2.0, 3.0));
        assert_eq!(p4, new_point(3.5, 2.0, 3.0));
    }

    #[test]
    fn transforming_a_ray() {
        let origin = new_point(1.0, 2.0, 3.0);
        let direction = new_vector(0.0, 1.0, 0.0);
        let ray = Ray::new(origin, direction);
        let transform = transformations::translate(3.0, 4.0, 5.0);
        let r2 = ray.transform(transform);
        assert_eq!(r2.origin(), new_point(4.0, 6.0, 8.0));
        assert_eq!(r2.direction(), new_vector(0.0, 1.0, 0.0));
    }

    #[test]
    fn scaling_a_ray() {
        let origin = new_point(1.0, 2.0, 3.0);
        let direction = new_vector(0.0, 1.0, 0.0);
        let ray = Ray::new(origin, direction);
        let transform = transformations::scale(2.0, 3.0, 4.0);
        let r2 = ray.transform(transform);
        assert_eq!(r2.origin(), new_point(2.0, 6.0, 12.0));
        assert_eq!(r2.direction(), new_vector(0.0, 3.0, 0.0));
    }
}

use super::color;
use super::tuple::{new_point, new_vector, Tuple};

pub struct Light {
    position: Tuple,
    intensity: color::Color,
}

impl Light {
    pub fn new(position: Tuple, intensity: color::Color) -> Light {
        return Light {
            position: position,
            intensity: intensity,
        };
    }

    pub fn position(&self) -> Tuple {
        return self.position;
    }

    pub fn intensity(&self) -> color::Color {
        return self.intensity;
    }
}

#[cfg(test)]
mod tests {
    use super::color::Color;
    use super::Light;
    use super::{new_point, new_vector};

    #[test]
    fn point_light_has_position_and_intensity() {
        let intensity = Color::new(1.0, 1.0, 1.0);
        let position = new_point(0.0, 0.0, 0.0);
        let light = Light::new(position, intensity);
        assert_eq!(light.position(), position);
        assert_eq!(light.intensity(), intensity);
    }
}

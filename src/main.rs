use std::fs;

mod features;
mod utils;

fn draw(canvas: &mut features::canvas::Canvas) {
    let wall_z = 10 as f32;
    let sphere_color = features::color::Color::new(1.0, 0.2, 1.0);
    let wall_size = 7 as f32;
    let ray_origin = features::tuple::new_point(0.0, 0.0, -5.0);

    let pixel_size = wall_size / canvas.width() as f32;
    let half = wall_size / 2_f32;

    let mut sphere = features::sphere::Sphere::new();
    let mut material = features::material::Material::new();
    material.color = sphere_color;
    sphere.material = material;

    let light_position = features::tuple::new_point(-10.0, 10.0, -10.0);
    let light_color = features::color::Color::new(1.0, 1.0, 1.0);
    let light = features::light::Light::new(light_position, light_color);

    for x in 0..canvas.width() {
        let world_x = half - pixel_size * x as f32;
        for y in 0..canvas.height() {
            let world_y = half - pixel_size * y as f32;
            let position = features::tuple::new_point(world_x, world_y, wall_z);
            let r = features::ray::Ray::new(ray_origin, (position - ray_origin).normalize());
            let intersects = sphere.intersects(r);
            let hit = intersects.hit();
            match hit {
                None => {}
                Some(hit) => {
                    let point = r.position_at(hit.t());
                    let normal = hit.object().normal_at(point);
                    let eye = -r.direction();
                    let material_color =
                        hit.object().material().lighting(point, &light, eye, normal);
                    canvas.write_pixel(x, y, material_color)
                }
            }
        }
    }
}

fn main() {
    let mut c = features::canvas::Canvas::new(300, 300);
    draw(&mut c);
    let ppm = c.to_ppm();
    fs::write("image.ppm", ppm).expect("Oh No");
}

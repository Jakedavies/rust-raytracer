use crate::features::tuple;
use assert_approx_eq::assert_approx_eq;

pub fn assert_tuple_equal(n: tuple::Tuple, m: tuple::Tuple) {
    assert_approx_eq!(n.x(), m.x(), 0.001);
    assert_approx_eq!(n.y(), m.y(), 0.001);
    assert_approx_eq!(n.z(), m.z(), 0.001);
    assert_approx_eq!(n.w(), m.w(), 0.001);
}
